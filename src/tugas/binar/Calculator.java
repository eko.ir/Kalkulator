package tugas.binar;

import java.util.ArrayList;
import java.util.Scanner;

class Calculator {
    private Scanner sc = new Scanner(System.in);
    private ArrayList<Double> nilai = new ArrayList<>();
    private double hasil;


    public void hitungPenjumlahan(ArrayList<Double> nilai) {
        hasil = 0;
        for (double nl : nilai) {
            hasil += nl;
        }
    }

    public void hitungPerkalian(ArrayList<Double> nilai) {
        hasil = nilai.get(0);
        for (int i = 1; i < nilai.size(); i++) {
            hasil *= nilai.get(i);
        }
    }

    public double getHasil() {
        return hasil;
    }


    void arrayNum(int arr[]) {

        int min = arr[0];
        int max = arr[0];
        int med = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }
            if (arr[i] > max) {
                max = arr[i];
            }
            med = arr[i] + med;
        }

        System.out.println("Nilai paling besar: " + max);
        System.out.println("Nilai paling kecil: " + min);
        System.out.println("Nilai rata-rata: " + med / arr.length);
    }

    public void inputNilai() {
        System.out.print("Masukkan nilai : ");
        nilai.add(sc.nextDouble());
    }

    public void menu() {
        System.out.println("========== MENU ========");
        System.out.println("1. Penjumlahan");
        System.out.println("2. Pengurangan");
        System.out.println("3. Pembagian");
        System.out.println("4. Perkalian");
        System.out.println("5. Nilai maks/min dan rata - rata");
        System.out.print("Masukkan pilihan : ");
        int pil = sc.nextInt();

        switch (pil) {
            case 1:
                inputNilai();
                inputNilai();
                hitungPenjumlahan(nilai);
                System.out.println("Hasil : " + getHasil());
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                inputNilai();
                inputNilai();
                hitungPerkalian(nilai);
                System.out.println("Hasil : " + getHasil());
                break;
            case 5:
                System.out.print("Masukkan jumlah angka yang ingin dimasukkan :");
                int jumlah = sc.nextInt();
                int arr[] = new int[jumlah];
                for (int i = 0; i < jumlah; i++) {
                    System.out.print("Nilai " + (i + 1) + " : ");
                    arr[i] = sc.nextInt();
                }
                arrayNum(arr);
                break;

        }
    }
}
